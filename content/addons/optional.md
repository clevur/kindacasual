---
title: "Optional"
date: 2018-09-02T19:15:35-07:00
anchor: "optional"
weight: 24
---

### More biased addons:

- [ElvUI](https://www.tukui.org/download.php?ui=elvui) - One stop shop bundled addon that provides the bulk of QoL changes players generally like.  Visit wago.io/elvui to try out some shared profiles. 
