---
title: "Addons"
date: 2018-09-02T19:15:35-07:00
anchor: "addons"
weight: 20
---

{{% block note %}}
Addons are packs of code that are maintained by the WoW community that add(on) to the base game functionality.  They usually improve different features of the game, or provide you an advantage during combat.
{{% /block %}}

Our biased opinion for managing addons is to use the [Twitch Desktop App](https://app.twitch.tv/download) as it will provide a single place to download, update, and customize your addons.

![Twitch](/twitch-addons.jpg)