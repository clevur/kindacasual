---
title: "General"
date: 2018-09-02T19:15:35-07:00
anchor: "general"
weight: 21
---

### A few addons that will make your life easier are:

- [Hekili](https://wow.curseforge.com/projects/hekili) - Hekili is a priority helper addon that can make recommendations several steps into the future.

- [LockoutChecker](https://wow.curseforge.com/projects/lockoutchecker) - displays mythic dungeon lockouts for group and guild members.

- [Pawn](https://wow.curseforge.com/projects/pawn) - helps you find upgrades for your gear.

- [World Quests List](https://wow.curseforge.com/projects/world-quests-list) - lists all world quests, filters, etc.

- [Details! Damage Meter](https://wow.curseforge.com/projects/details) - The BEST damage/healing/etc meter (Bobby said so!)