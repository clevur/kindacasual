---
title: "Advanced"
date: 2018-09-02T19:15:35-07:00
anchor: "advanced"
weight: 23
---

### For the try-hards:

- [WeakAuras 2](https://www.curseforge.com/wow/addons/weakauras-2) - allows you to display highly customizable graphics on your screen to indicate buffs, debuffs, and a whole host of similar types of information.  Visit wago.io/weakauras to find some import strings for your class and spec.

**WeakAura Imports:**

- [Bfa Dungeons](https://wago.io/r1lqyX32M) - Shows important spells/dots while in 5 mans.

- [Mythic+ Interrupt Tracker DG v2](https://wago.io/r1j5nW2NQ) - Tracks everyone's interrupts in your party. Tells you when they're ready, what the cooldown is and who used it.