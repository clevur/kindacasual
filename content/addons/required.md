---
title: "Required"
date: 2018-09-02T19:15:35-07:00
anchor: "required"
weight: 22
---

### (not really but... these are pretty defacto for players)

- [GTFO](https://wow.curseforge.com/projects/gtfo) - provides an audible alert when you're standing in something you aren't supposed to.

- [Deadly Boss Mods (DBM)](https://wow.curseforge.com/projects/deadly-boss-mods) - Boss Encounter alerts and raid tools.