---
title: "Try Hard - Uldir"
date: 2018-01-27T15:42:17+01:00
anchor: "tryhard_uldir"
weight: 50
---

We're Kinda Casual, so *naturally* also kinda try hard.  Let's cry together in raids.

Welcome to the first raid in BfA!

{{% block note %}}
Although there is no iLvl requirement to enter Uldir, you should be **330+** for this raid.

Anything less will negatively impact the raid group and significantly reduce our chances of downing bosses!
{{% /block %}}

Uldir, Halls of Control is a titan facility found in Nazmir, Zandalar.

![Uldir Bosses](/uldir.png)

Familiarize yourself with the following articles:

- [Wowhead: Uldir Raid Boss Abilities and Encounter Journal](https://www.wowhead.com/uldir-raid-boss-abilities-battle-for-azeroth)

- [Ready Check Pull: Uldir Raid Guide](https://readycheckpull.com/category/raidguides/uldir/)
