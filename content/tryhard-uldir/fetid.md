---
title: "Fetid Devourer"
date: 2018-09-02T19:15:35-07:00
anchor: "fetiddevourer"
weight: 23
---

{{% block note %}}
**DPS TL;DR**

**Don’t** stand on the tanks. 

Use nearby walls to your advantage for the **knockback stomp**. 

Kill **one of the adds** when they spawn. 

Dodge the **big breath** cone. 
{{% /block %}}

:point_right: :exclamation: **Read the [Strategy Guide - Fetid Devourer](https://readycheckpull.com/fetid-devourer-heroic-normal/) before the fight.** :exclamation: :point_left:

<iframe width="560" height="315" src="https://www.youtube.com/embed/DzlqGowoTbk?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/pPsgoqYrrHY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

![Fetid Devourer](/fetidminimal.png)