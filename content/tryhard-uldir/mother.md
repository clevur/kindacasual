---
title: "MOTHER"
date: 2018-09-02T19:15:35-07:00
anchor: "mother"
weight: 22
---

{{% block note %}}
**DPS TL;DR**

**Don’t** be in front of the boss. 

Dodge **fire swirlies**. Run against wind. Stand in safe gap of **laser beams**. 

Run through the blue barrier **when told**. Kill and interrupt adds **after** you pass through. 
{{% /block %}}

:point_right: :exclamation: **Read the [Strategy Guide - MOTHER](https://readycheckpull.com/mother-heroic-normal/) before the fight.** :exclamation: :point_left:

<iframe width="560" height="315" src="https://www.youtube.com/embed/UWr5-lKdOhY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/74luuvzAjFk?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

![MOTHER](/motherminimal.png)
