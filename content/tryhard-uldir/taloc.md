---
title: "Taloc"
date: 2018-09-02T19:15:35-07:00
anchor: "taloc"
weight: 21
---

{{% block note %}}
**DPS TL;DR**

If you get a **red debuff**, run it to the **closest wall** and **keep moving until it’s gone**.

When the boss runs away, **don’t follow him until after he does a big smash**.

**Kill red adds** on the elevator. **Avoid gray adds**.
{{% /block %}}

:point_right: :exclamation: **Read the [Strategy Guide - Taloc](https://readycheckpull.com/taloc-heroic-normal-guide/) before the fight.** :exclamation: :point_left:

<iframe width="560" height="315" src="https://www.youtube.com/embed/IEt-nIGCoyE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/UUdVwzZFwOE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

![Taloc](/talocminimal.png)