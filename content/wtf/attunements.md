---
title: "Attunements"
date: 2018-01-27T15:42:17+01:00
anchor: "attunements"
weight: 21
---

{{% block note %}}
Instance attunement refers to the process of gaining permanent access to an instance. The term comes from one of the first of such quests created: [Attunement to the Core](https://www.wowhead.com/quest=7848/attunement-to-the-core).
{{% /block %}}

Yup, attunements are still a thing.

Out of the 10 dungeons that launched with BfA, 2 require attunement.

**Use the [Wowhead: Battle for Azeroth Attunements Tool](https://www.wowhead.com/attunement).**

This will pull your character info from your [web profile](https://worldofwarcraft.com/en-us/character/wyrmrest-accord/Spankk) (example) and figure out where you are in the attunement process.

![Attunement](/attunement.png)

**TL:DR:**

- [Achievement: Zandalar Forever!](https://www.wowhead.com/achievement=12479/zandalar-forever) to unlock [King's Rest](https://www.wowhead.com/kings-rest)

- Continue [War Campaign](https://www.wowhead.com/horde-war-campaign) until 7.5k/12k Rep with [The Honorbound](https://www.wowhead.com/faction=2157) to unlock [Siege of Boralus](https://www.wowhead.com/siege-of-boralus)