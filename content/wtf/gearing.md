---
title: "Gearing"
date: 2018-01-27T15:42:17+01:00
anchor: "gearing"
weight: 20
---

Use [Addon: Pawn](https://wow.curseforge.com/projects/pawn) as you progress through Dailies and War Campaign to easily see what is an upgrade.

![Pawn](/pawn.jpg)

Reference [Wowhead: Gearing up in Battle for Azeroth](https://www.wowhead.com/gearing-up-in-battle-for-azeroth)

![Gearing](/gearing.png)

Do the following to get to reach iLvl 340 for Uldir:

1. Heroic Dungeons

2. Mythic Dungeons

3. [World Quests](https://www.wowhead.com/guides/battle-for-azeroth-world-quests)

4. [Emissary World Quests](https://www.wowhead.com/guides/battle-for-azeroth-world-quests#emissary-quests-and-rewards)