---
title: "Specialization"
date: 2018-01-27T15:42:17+01:00
anchor: "spec"
weight: 19
---

If you are DPS, you'll have some spec options -- and some are better than others.

Visit the https://www.wow-dps.com/wow-dps-ranking/ page and figure out which is most powerful, you should probably use that.

![Wowdps](/wowdps.png)

Next, get to know your spec.  You can cheat a bit and reference the following:

- [Icy Veins: Class Guides](https://www.icy-veins.com/wow/class-guides)

- [Wowhead: Class Guides](https://www.wowhead.com/classes)

Take specific note of rotations and talents.

Rely on [Addon: Hekili](https://wow.curseforge.com/projects/hekili) as you get a feel for your new spec rotation.

Look up your "S Tier" Azerite Effects.  For example: [Best Azerite Powers for Holy Priest](https://www.wowhead.com/guides/holy-priest-azerite-traits-powers-armor-bfa-battle-for-azeroth#best-azerite-powers-for-holy-priest)

![S Tier](/stier.png)