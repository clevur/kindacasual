---
title: "Introduction"
date: 2018-01-27T15:42:17+01:00
anchor: "introduction"
weight: 10
---

Greetings!

Welcome to the guild *website*.  This isn't your regular guild site with forums and calendars.  It was put together to answer questions for our members and serve as a reference guide as we progress through BfA.

![Cool.](/cool.jpg)