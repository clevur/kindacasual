---
title: "Flying"
date: 2018-09-02T19:15:35-07:00
anchor: "flying"
weight: 20
---

- [Battle for Azeroth Pathfinder: How to Unlock Flying in BFA](https://www.wowhead.com/battle-for-azeroth-pathfinder-how-to-unlock-flying-in-bfa)

Similar to the Attunement Tool, use the **[Wowhead: Battle for Azeroth Pathfinder Tool](https://www.wowhead.com/flying).**

![Flying](/flying.png)

You'll need to complete a two-phase achievement pair:

- [Battle for Azeroth Pathfinder, Part One](https://www.wowhead.com/achievement=12989)

- [Battle for Azeroth Pathfinder, Part Two](https://www.wowhead.com/achievement=11446)